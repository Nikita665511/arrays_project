function flatten(elements,depth=1) {

    let array=[];
    for(let index=0;index<elements.length;index++){
        if(Array.isArray(elements[index]) && depth >=1){
            array=array.concat(flatten(elements[index],depth-1));
        }
        else {
            if(elements[index]!=undefined)
            {
                array.push(elements[index]);
            }
        }
    }
    return array;
}
module.exports = flatten;


     

