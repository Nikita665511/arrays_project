
function find(elements = [], cb) {
  if (elements instanceof Array && cb instanceof Function) {
      for (let i = 0; i < elements.length; i++) {
          let element = elements[i];
          if (cb(element)) {
              return element;
          }
      }
  }
}

module.exports = find;

