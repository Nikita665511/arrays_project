function filter(elements = [], cb = (x) => true) {
    let filter_elements = [];
    if (elements && elements instanceof Array && cb && cb instanceof Function) {
        for (let index = 0; index < elements.length; index++) {
            let element = elements[index];
            if 
            (cb(element,index,elements) == true) {
                filter_elements.push(element);
            }
        }
    }
    return filter_elements;
}
module.exports = filter;