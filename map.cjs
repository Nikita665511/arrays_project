function map(elements ,cb = x => x){
    let res = [];
    if(elements && elements instanceof Array && cb && cb instanceof Function){
    
        for(let index =0;index<elements.length;index++){
            res.push(cb(elements[index],index,elements));
        
}
    return res;
}
}

module.exports = map;