function each(elements = [], cb){
    if (elements instanceof Array && cb instanceof Function) {
        for(let index=0;index<elements.length;index++)
            cb(elements[index]);
    }
}
module.exports = each;

 